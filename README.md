# 智能家居系统

#### 介绍
基于树莓派的智能家居系统的实现方案，用户可以通过手机网页的控制界面，通过WiFi技术加上STM32单片机实现对住宅设施的远程监控，包括灯光的控制、环境（温湿度）的监测、视频监控、硬件WIFI的设置以及硬件开机、重启等功能；
本系统主要以OneNet中国移动物联网开发者平台为核心搭建

#### 软件架构
软件架构说明


#### 截图演示
![输入图片说明](https://images.gitee.com/uploads/images/2020/0501/163255_c1de817f_5655643.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0501/163301_a9ed2771_5655643.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0501/163308_3ea931b0_5655643.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0501/163311_c25a5d59_5655643.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0501/163436_521f91a9_5655643.png "屏幕截图.png")
