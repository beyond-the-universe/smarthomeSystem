/*
Navicat MySQL Data Transfer

Source Server         : 本地mysql
Source Server Version : 50556
Source Host           : localhost:3306
Source Database       : canhuanet

Target Server Type    : MYSQL
Target Server Version : 50556
File Encoding         : 65001

Date: 2020-05-16 12:48:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `apikey`
-- ----------------------------
DROP TABLE IF EXISTS `apikey`;
CREATE TABLE `apikey` (
  `id` char(9) NOT NULL,
  `apikey` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of apikey
-- ----------------------------
INSERT INTO `apikey` VALUES ('578095058', 'H9gWQeBs=ZhfNh3Za6RNGE9dMio=');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `pwd` varchar(10) NOT NULL,
  `deviceId` char(9) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_device` (`deviceId`),
  CONSTRAINT `fk_device` FOREIGN KEY (`deviceId`) REFERENCES `apikey` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '张三', '12345678', '578095058');
INSERT INTO `user` VALUES ('2', '李四', '12345678', '578095058');
